//SOUNDS
var audio = [], typingText = [],
	audioPieces = [];

//TIMEOUTS
var timeout = [], timer,
	sprite = [],
	globalName,
	tincan;

//AWARDs NUMBER
var awardNum = 0;

//FUNCTIONS

var launch = [];

var startTimer = function(jqueryElement, secondsLeft, callBack)
{
	if(secondsLeft <= 0)
		callBack();
	else
	{
		secondsLeft --;
		var minutesLeft = Math.floor(secondsLeft / 60);
		var result = "";
		secondsLeft = Math.round(secondsLeft % 60);
		
		if(minutesLeft > 9) 
			result = "" + minutesLeft + ":";
		else
			result = "0" + minutesLeft + ":";
		
		if(secondsLeft > 9)
			result += secondsLeft;
		else 
			result += "0" + secondsLeft;
		
		jqueryElement.html(result);
		
		secondsLeft += (minutesLeft * 60);
		
		timer = setTimeout(function(){
			startTimer(jqueryElement, secondsLeft, callBack);
		}, 1000);
	}
}

var stopTimer = function()
{
	clearTimeout(timer);
}

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
	
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
}

AudioPiece.prototype.play = function()
{
	this.audio.currentTime = this.start;
	this.audio.play();
}

AudioPiece.prototype.pause = function()
{
	this.audio.pause();
}

AudioPiece.prototype.addEventListener = function(e, callBack)
{
	var currPiece = this,
		currAudio = this.audio,
		currDuration = Math.round(currAudio.duration);
	
	if(e === "ended")
	{
		console.log("else end: " + currPiece.end + ", " + currDuration);
		currAudio.addEventListener("timeupdate", function(){
			var currTime = Math.round(currAudio.currentTime);
				
			if(currTime === currPiece.end)
			{
				currAudio.pause();
				currAudio.currentTime += 3;
				callBack();
			}
		});
	}
	if(e === "timeupdate")
	{
		currPiece.audio.addEventListener("timeupdate", callBack);
	}
}

AudioPiece.prototype.getStart = function()
{
	return this.start;
}

var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'n')
		{
			tObj.jqueryElement.append("<br>");
			tObj.currentSymbol += 2;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 's')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<sup>" + tObj.text[tObj.currentSymbol] + "</sup>");
			tObj.currentSymbol ++;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'b')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
			tObj.write();
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum);
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("outline", "3px solid red");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("outline", "");
			if (times) 
				blink(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

var blink2 = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("box-shadow", "0 0 3pt 2pt orange");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("box-shadow", "");
			if (times) 
				blink2(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

var setLRSData = function(){
	tincan = new TinCan (
    {
        recordStores: [
            {
                "endpoint": "http://54.154.57.220/data/xAPI/",
				"username": "e083498f4e256e68ab2c5ae2be4195d9a348eb20",
				"password": "c6ea3c32a9d77919d0eb3cdea3bc5d460f50d93b"
            }
        ]
    });
}

var sendLaunchedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/launched"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var sendCompletedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz/chemistry"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/completed"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var setMenuStuff = function(){
	var prefix = "#frame-000 ",
		regBox = $(".reg-box"),
		error = $(".error"),
		enterButton = $(".enter-button"),
		password = $(".password"),
		name = $(".name"),
		annotation = $("#frame-000 .annotation-hidden"),
		annotationButton = $("#frame-000 .annotation-button");
	
	name.val("");
	password.val("");
	
	var sayHello = function(){
		regBox.html("Здравствуйте, " + name.val() + "!");
		regBox.css("height", "3em");
		regBox.css("padding", "0.5em");
		regBox.css("text-align", "center");
		globalName = name.val();
		regBox.addClass("topcorner");
		timeout[0] = setTimeout(function(){
			regBox.html(name.val());
			regBox.css("width", name.val().length + "em");
		}, 3000);
	};
	
	if(globalName)
		sayHello();
	
	var annotationButtonListener = function(){
		annotation.toggleClass("annotation-shown");
		annotationButton.toggleClass("annotation-button-close");

	};
	annotationButton.off("click", annotationButtonListener);
	annotationButton.on("click", annotationButtonListener);
	
	var enterButtonListener = function(){
		if(password.val() === "12345")
			sayHello();
		else
			error.html("неверный пароль");
	};
	enterButton.off("click", enterButtonListener);
	enterButton.on("click", enterButtonListener);
}

launch["frame-000"] = function(){
}

launch["frame-101"] = function(){
	theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		cloud = $(prefix + ".cloud");
	
	audio[0] = new Audio("audio/nature.mp3"),
	audio[1] = new Audio("audio/elevator.mp3"),
	audio[2] = new Audio("audio/s1-1.mp3");
	
	doneSecond = 0;
		
	cloud.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	bg.addClass("transition-2s");
	
	var functions = [];
	
	functions[0] = function(){
		audio[0].volume = 0.2;
		bg.css("background-position", "3% 80%");
		bg.css("background-size", "500%")
	};
	
	functions[2] = function(){
		bg.css("background-position", "40% 80%");
		bg.css("background-size", "300%");
	};
		
	functions[5] = function(){
		cloud.fadeOut(0);
		bg.css("background-position", "99% 10%");
		bg.css("background-size", "450%");
	};
	functions[6] = function(){
		bg.css("background-position", "72% 25%");
		bg.css("background-size", "500%");
	};
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		audio[0].play();
		bg.fadeIn(1000);
		timeout[0] = setTimeout(functions[0], 4000);
		timeout[1] = setTimeout(function(){
			audio[1].currentTime = 0;
			audio[1].play();
		}, 5000);
		timeout[2] = setTimeout(functions[2], 9000);
		timeout[3] = setTimeout(function(){
			audio[0].volume = 0.1; 
			audio[2].currentTime = 0;
			audio[2].play();
			cloud.fadeIn(500);
		}, 13000);
		timeout[4] = setTimeout(functions[5], 19000);
		timeout[5] = setTimeout(functions[6], 25000);
		timeout[6] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			sendCompletedStatement(1);
			fadeNavsInAuto();
		}, 28000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 2000);
}

launch["frame-102"] = function(){ 
	theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		processes = $(prefix + ".process"),
		playButtons = $(prefix + ".process .fa-play");	
		
	audio[0] = new Audio("audio/s2-1.mp3");
	audio[1] = new Audio("audio/s2-1-1.mp3");
	
	audioPiece = [];
	audioPiece["condensation-rus"] = new AudioPiece("s2-1", 0, 5);
	audioPiece["condensation-kaz"] = new AudioPiece("s2-1", 0, 5);
	audioPiece["condensation-eng"] = new AudioPiece("s2-1", 0, 5);
	
	audioPiece["evaporation-rus"] = new AudioPiece("s2-1", 4, 8);
	audioPiece["evaporation-kaz"] = new AudioPiece("s2-1", 4, 8);
	audioPiece["evaporation-eng"] = new AudioPiece("s2-1", 4, 8);
	
	audioPiece["freezing-rus"] = new AudioPiece("s2-1", 7, 12);
	audioPiece["freezing-kaz"] = new AudioPiece("s2-1", 7, 12);
	audioPiece["freezing-eng"] = new AudioPiece("s2-1", 7, 12);
	
	audioPiece["boiling-rus"] = new AudioPiece("s2-1", 12, 17);
	audioPiece["boiling-kaz"] = new AudioPiece("s2-1", 12, 17);
	audioPiece["boiling-eng"] = new AudioPiece("s2-1", 12, 17);
	
	audioPiece["cooling-rus"] = new AudioPiece("s2-1", 17, 23);
	audioPiece["cooling-kaz"] = new AudioPiece("s2-1", 17, 23);
	audioPiece["cooling-eng"] = new AudioPiece("s2-1", 17, 23);
	
	audioPiece["heat-rus"] = new AudioPiece("s2-1", 22, 27);
	audioPiece["heat-kaz"] = new AudioPiece("s2-1", 22, 27);
	audioPiece["heat-eng"] = new AudioPiece("s2-1", 22, 27);
	
	audioPiece["temperature-rus"] = new AudioPiece("s2-1", 27, 35);
	audioPiece["temperature-kaz"] = new AudioPiece("s2-1", 27, 35);
	audioPiece["temperature-eng"] = new AudioPiece("s2-1", 27, 35);
	
	processes.fadeOut(0);
	playButtons.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	var playButtonsListener = function(){
		var parent = $(this).parent();
		audioPiece[parent.attr("id")].play();
	};
	playButtons.off("click", playButtonsListener);
	playButtons.on("click", playButtonsListener);
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		bg.fadeIn(1000);
		audio[1].play();
		timeout[0] = setTimeout(function(){ audio[0].play(); }, 5000);
		timeout[1] = setTimeout(function(){	$(processes[0]).fadeIn(500); }, 5500); //condensation
		timeout[2] = setTimeout(function(){	$(processes[1]).fadeIn(500); }, 6500);
		timeout[3] = setTimeout(function(){ $(processes[2]).fadeIn(500); }, 7500);
		timeout[4] = setTimeout(function(){	audio[0].pause(); }, 9000);
		timeout[5] = setTimeout(function(){ audio[0].play(); }, 12000);
		timeout[6] = setTimeout(function(){ $(processes[3]).fadeIn(500); }, 12500); //evaporation
		timeout[7] = setTimeout(function(){ $(processes[4]).fadeIn(500); }, 13500);
		timeout[8] = setTimeout(function(){ $(processes[5]).fadeIn(500); }, 14500);
		timeout[9] = setTimeout(function(){	audio[0].pause(); }, 15000);
		timeout[10] = setTimeout(function(){ audio[0].play(); }, 18000);
		timeout[11] = setTimeout(function(){ $(processes[6]).fadeIn(500); }, 19500); //freezing
		timeout[12] = setTimeout(function(){ $(processes[7]).fadeIn(500); }, 20500);
		timeout[13] = setTimeout(function(){ $(processes[8]).fadeIn(500); }, 21500);
		timeout[14] = setTimeout(function(){ audio[0].pause(); }, 23000);
		timeout[15] = setTimeout(function(){ audio[0].play(); }, 26000);
		timeout[16] = setTimeout(function(){ $(processes[9]).fadeIn(500); }, 26500); //boiling
		timeout[17] = setTimeout(function(){ $(processes[10]).fadeIn(500); }, 27500);
		timeout[18] = setTimeout(function(){ $(processes[11]).fadeIn(500); }, 28500);
		timeout[19] = setTimeout(function(){ audio[0].pause(); }, 30000);
		timeout[20] = setTimeout(function(){ audio[0].play(); }, 33000);
		timeout[21] = setTimeout(function(){ $(processes[12]).fadeIn(500); }, 35500); //cooling
		timeout[22] = setTimeout(function(){ $(processes[13]).fadeIn(500); }, 36500);
		timeout[23] = setTimeout(function(){ $(processes[14]).fadeIn(500); }, 37500);
		timeout[24] = setTimeout(function(){ audio[0].pause(); }, 39000);
		timeout[25] = setTimeout(function(){ audio[0].play(); }, 42000);
		timeout[26] = setTimeout(function(){ $(processes[15]).fadeIn(500); }, 43500); //heat
		timeout[27] = setTimeout(function(){ $(processes[16]).fadeIn(500); }, 44500);
		timeout[28] = setTimeout(function(){ $(processes[17]).fadeIn(500); }, 45500);
		timeout[29] = setTimeout(function(){ audio[0].pause(); }, 46500);
		timeout[30] = setTimeout(function(){ audio[0].play(); }, 49500);
		timeout[31] = setTimeout(function(){ $(processes[18]).fadeIn(500); }, 51000); //temperature
		timeout[32] = setTimeout(function(){ $(processes[19]).fadeIn(500); }, 52000);
		timeout[33] = setTimeout(function(){ $(processes[20]).fadeIn(500); }, 53000);
		timeout[34] = setTimeout(function(){ playButtons.fadeIn(500); }, 55000);
		timeout[35] = setTimeout(function(){ 
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto(); 
			sendCompletedStatement(2);
		}, 57000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(2);
	}, 2000);
}

launch["frame-103"] = function(){
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		processes = $(prefix + ".process"),
		title = $(prefix + ".title");
	
	fadeNavsOut();
	fadeLauncherIn();
	title.fadeOut(0);
	
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	
	audioPieces[0] = new AudioPiece("s3-1", 0, 8);
	audioPieces[1] = new AudioPiece("s3-1", 11, 20);
	
	var processesListener = function(){
		audio[2].play();
		$(this).toggleClass("active");
		
		var activeSpans = $(".active");
		
		if (activeSpans.length === 3)
		{
			dataProcess = [$(activeSpans[0]).attr("data-process"), $(activeSpans[1]).attr("data-process"),
							$(activeSpans[2]).attr("data-process")];
			if((dataProcess[0] === dataProcess[1]) && (dataProcess[1] === dataProcess[2]))
			{
				audio[0].play();
				activeSpans.remove()
				
				if(!$(prefix + ".process").length)
				{
					audioPieces[1].play();
					theFrame.attr("data-done", "true"); 
					fadeNavsInAuto();
					sendCompletedStatement(3);
				}	
			}
			else
			{
				activeSpans.removeClass("active");
				activeSpans.addClass("error");
				audio[1].play();
				setTimeout(function(){activeSpans.removeClass("error");}, 1000);
			}				
		}
	};
	processes.off("click", processesListener);
	processes.on("click", processesListener);
	processes.fadeOut(0);
	audioPieces[0].addEventListener("ended", function(){
		title.fadeIn(1000);
		processes.fadeIn(1000);
	});
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		bg.fadeIn(1000);
		timeout[0] = setTimeout(function(){
			audioPieces[0].play();
		}, 2000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(3);
	}, 2000);
};

launch["frame-104"] = function(){
	theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		balls = $(prefix + ".ball");
		
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	audioPieces[0] = new AudioPiece("s4-1", 0, 3);
		
	balls.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	var ballsListener = function(){
		audio[2].play();
	};
	balls.off("click", ballsListener);
	balls.on("click", ballsListener);
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	}
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		basket.css("background-color", vegetable.css("background-color"));
		basket.css("color", "white");
		vegetable.remove();
		audio[0].play();
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css({
			"left": "", 
			"top": ""
		});
		audio[1].play();
	}
	
	var finishCondition = function()
	{
		return !$(prefix + ".ball").length;
	}
	
	var finishFunction = function(){
		timeout[0] = setTimeout(function(){
			fadeNavsInAuto();
			sendCompletedStatement(4);
		}, 1000);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		audioPieces[0].play();
		balls.fadeIn(1000);
		bg.fadeIn(1000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(4);
	}, 2000);
};

launch["frame-105"] = function(){
	theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		balls = $(prefix + ".ball");
		
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	audioPieces[0] = new AudioPiece("s4-1", 3, 9);
		
	balls.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	var ballsListener = function(){
		audio[2].play();
	};
	balls.off("click", ballsListener);
	balls.on("click", ballsListener);
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	}
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		basket.css("background-color", vegetable.css("background-color"));
		basket.css("color", "white");
		vegetable.remove();
		audio[0].play();
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css({
			"left": "", 
			"top": ""
		});
		audio[1].play();
	}
	
	var finishCondition = function()
	{
		return !$(prefix + ".ball").length;
	}
	
	var finishFunction = function(){
		timeout[0] = setTimeout(function(){
			fadeNavsInAuto();
			sendCompletedStatement(5);
		}, 1000);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		audioPieces[0].play();
		balls.fadeIn(1000);
		bg.fadeIn(1000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(5);
	}, 2000);
};

launch["frame-106"] = function(){
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		medeo = $(prefix + ".bg-base"),
		iceSkater = $(prefix + ".ice-skater"),
		cloudAndRain = $(prefix + ".cloud-and-rain"),
		medeoEvaporation = $(prefix + ".medeo-evaporation"),
		matter = $(prefix + ".elem"),
		sugar = $(prefix + ".sugar"), 
		shampoo = $(prefix + ".shampoo"),
		juice = $(prefix + ".juice"),
		juiceAnimated = $(prefix + ".juice-animated"), 
		oil = $(prefix + ".oil"), 
		gas = $(prefix + ".gas"),
		gasSpread = $(prefix + ".gas-spread");
	
	audio[0] = new Audio("audio/s5-1.mp3");
	audio[1] = new Audio("audio/rain.mp3");
	audio[2] = new Audio("audio/ice-skating.mp3");
	
	sprite[0] = new Motio(juiceAnimated[0], {
		"fps": "7",
		"frames": "13"
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	//PRE-LOADING IMAGES
	imageGas = new Image();
	imageGas.src = "pics/s6-gas.gif";
	
	imageGas2 = new Image();
	imageGas2.src = "pics/s6-gas-spread.gif";
	
	var currentSecond = 0;
	
	matter.fadeOut(0);
	medeo.fadeIn(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 1000);
		timeout[1] = setTimeout(function(){
			audio[0].pause(); 
			audio[2].play();
			medeo.css("backgroundSize", "400%");
			medeo.css("backgroundPosition", "50% 90%");
		}, 10300);
		timeout[2] = setTimeout(function(){
			iceSkater.fadeIn(0);
			iceSkater.css("backgroundPosition", "70% 70%");
		}, 10800);
		timeout[3] = setTimeout(function(){
			audio[0].play();
			iceSkater.fadeOut(0);
			medeo.css("backgroundSize", ""); 
			medeo.css("backgroundPosition", "");
		}, 13800);
		timeout[4] = setTimeout(function(){
			audio[0].pause(); 
			audio[1].play();
			cloudAndRain.fadeIn(0);
			cloudAndRain.css("opacity", "0.5");
		}, 14800);
		timeout[5] = setTimeout(function(){
			cloudAndRain.fadeOut(0);
			audio[0].play();
		}, 17800);
		timeout[6] = setTimeout(function(){
			audio[0].pause(); 
			medeo.css("backgroundSize", "350%");
			medeo.css("backgroundPosition", "70% 105%");
			medeoEvaporation.css("backgroundSize", "350%");
			medeoEvaporation.css("backgroundPosition", "80% 95%");
		}, 19300); 
		timeout[7] = setTimeout(function(){
			medeoEvaporation.fadeIn(300);	
		}, 20000);
		timeout[8] = setTimeout(function(){
			audio[0].play();
		}, 21000);
		timeout[9] = setTimeout(function(){
			matter.fadeOut(1000);
		}, 23500);
		timeout[10] = setTimeout(function(){
			sugar.fadeIn(300);
		}, 24500);
		timeout[11] = setTimeout(function(){
			sugar.css("background-position", "-150% 0%"); 
			shampoo.fadeIn(1000);
		}, 33500);
		timeout[12] = setTimeout(function(){
			shampoo.css("background-position", "0% 0%") 
			oil.fadeIn(300);
		}, 34500);
		timeout[13] = setTimeout(function(){
			juice.fadeIn(0);
			juice.css("background-position", "95% 0%");
		}, 36500);
		timeout[14] = setTimeout(function(){
			oil.fadeOut(500);
			shampoo.fadeOut(500);
			juice.fadeOut(500);
		}, 37500);
		timeout[15] = setTimeout(function(){
			juiceAnimated.fadeIn(0);
			sprite[0].play();
		}, 39500);
		timeout[16] = setTimeout(function(){
			juiceAnimated.fadeOut(200);
			gas.fadeIn(0);
			gas.css("background-image", "url('pics/s6-gas.gif')");
		}, 44500);
		timeout[17] = setTimeout(function(){
			gas.fadeOut(0);
			gasSpread.fadeIn(0);
			gasSpread.css("background-image", "url('pics/s6-gas-spread.gif')"); 
		}, 50500);
		timeout[18] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(6);
		}, 55500);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(6);
	}, 2000);
}

launch["frame-107"] = function(){
	theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		coolGraph = $(prefix + ".cool.graph"),
		heatGraph = $(prefix + ".heat.graph"),
		coolTitle = $(prefix + ".cool.title"),
		heatTitle = $(prefix + ".heat.title"),
		bg = $(prefix + ".bg");
		
	var coolGraphSprite = new Motio(coolGraph[0], {
		"fps": 1,
		"frames": 9
	});
	coolGraphSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	var heatGraphSprite = new Motio(heatGraph[0], {
		"fps": "1",
		"frames": "10"
	});
	heatGraphSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	audio[0] = new Audio("audio/s6-1.mp3");
	
	coolGraph.fadeOut(0);
	coolTitle.fadeOut(0);
	heatTitle.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		audio[0].play();
		timeout[0] = setTimeout(function(){
			bg.fadeOut(1000);
		}, 11000);
		timeout[0] = setTimeout(function(){
			audio[0].currentTime = 15;
			heatTitle.fadeIn(2000);
		}, 12000);
		timeout[1] = setTimeout(function(){
			heatGraphSprite.play();
		}, 14000);
		timeout[2] = setTimeout(function(){
			audio[0].currentTime = 28;
			heatTitle.fadeOut(2000);
			heatGraph.fadeOut(2000);
			coolTitle.fadeIn(2000);
			coolGraph.fadeIn(2000);
		}, 25000);
		timeout[3] = setTimeout(function(){
			coolGraphSprite.play();
		}, 28000);
		timeout[4] = setTimeout(function(){
			audio[0].currentTime = 36;
			coolGraphSprite.play();
		}, 33000);
		timeout[5] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(7);
		}, 42000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(7);
	}, 2000);
};

launch["frame-108"] = function(){
	theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		graph = $(prefix + ".graph"),
		tasks = $(prefix + ".task"),
		answers = $(prefix + ".answer"),
		activeTask = 1, 
		correctNum = 0, 
		result = $(prefix + ".task-4 .question");
	
	audioPieces[0] = new AudioPiece("s7-1", 0, 3);
	audioPieces[1] = new AudioPiece("s7-1", 3, 6);
	audioPieces[2] = new AudioPiece("s7-1", 6, 10);
	audioPieces[3] = new AudioPiece("s7-1", 10, 99);
	
	audioPieces[0].addEventListener("ended", function(){
		audioPieces[1].play();
		$(".task-" + activeTask).fadeIn(2000);
	});
	
	var answersListener = function(){
		var currButton = $(this);
		if(currButton.hasClass("correct"))
		{
			currButton.css("background-color", "green");
			correctNum ++;
		}
		else
		{
			currButton.css("background-color", "red");
			$(".task-" + activeTask + " .correct").css("background-color", "green");
		}
		
		result.html("Correct answers: " + correctNum);
		
		timeout[0] = setTimeout(function(){
			activeTask ++;
			tasks.fadeOut(1000);
			$(".task-" + activeTask).fadeIn(1000);
			
			if(activeTask === 4)
				timeout[1] = setTimeout(function(){
					fadeNavsInAuto();
					sendCompletedStatement(8);
				}, 3000);
			else
				audioPieces[activeTask].play();
		}, 4000);
	}
	answers.off("click", answersListener);
	answers.on("click", answersListener);
	
	tasks.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		audioPieces[0].play();
		graph.fadeIn(1000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(8);
	}, 2000);
}

launch["frame-109"] = function(){
	theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
			task = $(prefix + ".task"),
			sketchpadContainer = $(prefix + ".sketchpad-container"),
			sketchpad = $(prefix + ".responsive-sketchpad").sketchpad(
			{
				aspectRatio: 2/1            // (Required) To preserve the drawing, an aspect ratio must be specified
			});	
	
	sketchpad.attr("width", sketchpadContainer.css("width"));
	sketchpad.attr("height", sketchpadContainer.css("height"));
	$(window).resize(function(){
		sketchpad.attr("width", sketchpadContainer.css("width"));
		sketchpad.attr("height", sketchpadContainer.css("height"));
	});
	
	sketchpad.setLineColor('#177EE5');
	sketchpad.setLineSize(2);
	
	$(".control").click(function(){
		switch($(this).attr("id"))
		{
			case "clear":
				sketchpad.clear();
			break;
			case"undo":
				sketchpad.undo();
			break;
			case "red":
				sketchpad.setLineColor('#D5000D');
			break;
			case "blue":
				sketchpad.setLineColor('#177EE5');
			break;
			case "download":
			var canvas = document.querySelector("#frame-109 .responsive-sketchpad");
				var context = canvas.getContext("2d");
				context.fillStyle = "green";
				window.open(canvas.toDataURL("image/png"), '_blank');
				theFrame.attr("data-done", "true"); 
				fadeNavsInAuto();
				sendCompletedStatement(9);
			break;
		}
	});
	
	task.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		task.fadeIn(0);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(9);
	}, 2000);
}

launch["frame-110"] = function(){
	theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		elems = $(prefix + ".elem"),
		molecules = $(prefix + ".molecule"),
		glasses = $(prefix + ".glass"),
		waterGlass = $(prefix + ".glass.water"),
		iceGlass = $(prefix + ".glass.ice"),
		vaporGlass = $(prefix + ".glass.vapor"),
		iceStructure = $(prefix + ".ice.structure"),
		waterStructure = $(prefix + ".water.structure"),
		vaporStructure = $(prefix + ".vapor.structure"),
		structures = $(prefix + ".structure");
		
	audioPieces[0] = new AudioPiece("s9-1", 0, 14);
	audioPieces[1] = new AudioPiece("s9-1", 17, 24);
	audioPieces[2] = new AudioPiece("s9-1", 28, 35);
	audioPieces[3] = new AudioPiece("s9-1", 39, 50);
	audioPieces[4] = new AudioPiece("s9-1", 54, 99);
	
	fadeNavsOut();
	fadeLauncherIn();
	structures.fadeOut(0);
	glasses.fadeOut(0);
	
	audioPieces[0].addEventListener("ended", function(){
		//molecules.removeClass("center");
		audioPieces[1].play();
	});
	
	audioPieces[1].addEventListener("ended", function(){
		molecules.fadeOut(0);
		audioPieces[2].play();
		timeout[2] = setTimeout(function(){
			iceGlass.fadeIn(1000);
		}, 1000);
		timeout[3] = setTimeout(function(){
			iceGlass.addClass("big");
		}, 4000);
		timeout[4] = setTimeout(function(){
			iceStructure.fadeIn(0);
			iceStructure.addClass("big");
		}, 6000);		
	});
	audioPieces[2].addEventListener("ended", function(){
		audioPieces[3].play();
		iceGlass.fadeOut(500);
		iceStructure.fadeOut(500);
		timeout[5] = setTimeout(function(){
			waterGlass.fadeIn(1000);
		}, 1000);
		timeout[6] = setTimeout(function(){
			waterGlass.addClass("big");
		}, 4000);
		timeout[7] = setTimeout(function(){
			waterStructure.fadeIn(0);
			waterStructure.addClass("big");
		}, 6000);	
	});
	audioPieces[3].addEventListener("ended", function(){
		audioPieces[4].play();
		waterGlass.fadeOut(500);
		waterStructure.fadeOut(500);
		timeout[8] = setTimeout(function(){
			vaporGlass.fadeIn(1000);
		}, 1000);
		timeout[9] = setTimeout(function(){
			vaporGlass.addClass("big");
		}, 4000);
		timeout[10] = setTimeout(function(){
			vaporStructure.fadeIn(0);
			vaporStructure.addClass("big");
		}, 6000);
		timeout[11] = setTimeout(function(){
			fadeNavsInAuto();
			sendCompletedStatement(10);
		}, 13000);		
	});
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		audioPieces[0].play();
		timeout[0] = setTimeout(function(){
			molecules.fadeIn(1000);
		}, 2000);
		timeout[1] = setTimeout(function(){
			molecules.addClass("center");
		}, 5000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(10);
	}, 2000);
}

launch["frame-111"] = function(){
	theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		stateChange = $(prefix + ".state-change"),
		structures = $(prefix + ".structure"),
		iceStructure = $(prefix + ".ice-structure"),
		waterStructure = $(prefix + ".water-structure"),
		vaporStructure = $(prefix + ".vapor-structure");

	audioPieces[0] = new AudioPiece("s10-1", 0, 6);
	audioPieces[1] = new AudioPiece("s10-1", 10, 99);
	
	audioPieces[0].addEventListener("ended", function(){
		audioPieces[1].play();
		timeout[1] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(11);
		}, 20000);
	});
	
	var stateChangeSprite = new Motio(stateChange[0], {
		"fps": "1",
		"frames": "15"
	});
	
	stateChangeSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	structures.css("opacity", "0");
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		stateChange.fadeIn(1000);
		audioPieces[0].play();
		timeout[0] = setTimeout(function(){
			stateChangeSprite.play();
		}, 2000);
		timeout[1] = setTimeout(function(){
			iceStructure.css("opacity", "1");
		}, 4000);
		timeout[1] = setTimeout(function(){
			waterStructure.css("opacity", "1");
		}, 9000);
		timeout[2] = setTimeout(function(){
			vaporStructure.css("opacity", "1");
		}, 15000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(11);
	}, 2000);
};

launch["frame-112"] = function(){
	theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		balls = $(prefix + ".ball"),
		baskets = $(prefix + ".basket");

	audio[0] = new Audio("audio/s11-1.mp3");
		
	var successCondition = function(vegetable, basket){
		return vegetable === basket
	}
	
	var successFunction = function(vegetable, basket){
		vegetable.remove();
		basket.css("border-color", "green");
	}
	
	var failFunction = function(vegetable, basket){
		vegetable.css({
			"top": "", 
			"left": ""
		});
	}
	
	var finishCondition = function(){
		return !$(prefix + ".ball").length
	}
	
	var finishFunction = function(){
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(12);
		}, 2000);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		audio[0].play();
		timeout[0] = setTimeout(function(){
			blink2(balls, 500, 3)
		}, 1000);
		timeout[0] = setTimeout(function(){
			blink2(baskets, 500, 3)
		}, 3000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(12);
	}, 2000);
}

launch["frame-113"] = function(){
	theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		videoContainer = $(prefix + ".video-container"),
		video = $(prefix + ".video");
		
	audio[0] = new Audio("audio/s12-1.mp3");
	
	fadeNavsOut();
	fadeLauncherIn();
	
	video.attr("width", videoContainer.css("width"));
	video.attr("height", videoContainer.css("height"));
	
	$(window).resize(function(){
		video.attr("width", videoContainer.css("width"));
		video.attr("height", videoContainer.css("height"));
	});
	
	video[0].addEventListener("ended", function(){
		theFrame.attr("data-done", "true"); 
		fadeNavsInAuto();
		sendCompletedStatement(13);
	});
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		video[0].play();
		audio[0].play();
		timeout[0] = setTimeout(function(){
			audio[0].pause();
		}, 6000);
		timeout[1] = setTimeout(function(){
			audio[0].play();
		}, 35000);
		timeout[0] = setTimeout(function(){
			audio[0].pause();
		}, 42000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(13);
	}, 2000);
}

launch["frame-114"] = function(){
	theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		thermometer = document.querySelector(prefix + ".thermometer-animated"), 
		moleculesOn = document.querySelector(prefix + ".molecules-animated-on"),
		moleculesOff = document.querySelector(prefix + ".molecules-animated-off"),
		teapotOn = document.querySelector(prefix + ".teapot-on"),
		teapotOff = document.querySelector(prefix + ".teapot-off"),
		teapotAnimated = document.querySelector(prefix + ".teapot-animated"),
		turnOnButton = document.querySelector(prefix + ".turn-on-button"),
		turnOffButton = document.querySelector(prefix + ".turn-off-button"),
		allParts = $(".elem");
	
	allParts.fadeOut(0);
	$(thermometer).fadeIn(0);
	$(teapotOff).fadeIn(0);
	$(moleculesOff).fadeIn(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/kettle-boil-2.mp3");
	audio[1] = new Audio("audio/s12-1.mp3")
	
	var sprtTh = new Motio(thermometer, {
		"fps": "3", 
		"frames": "12"
	});
	sprtTh.on("frame", function(){
		if(sprtTh.frame === sprtTh.frames - 1)
		{
			sprtTh.pause();
			$(teapotOn).fadeOut(0);
			$(teapotAnimated).fadeIn(0);
			$(moleculesOff).fadeOut(0);
			$(moleculesOn).fadeIn(0);
			sprtMOn = new Motio(moleculesOn, {
				"fps": "1", 
				"frames": "14"
			});
			sprtMOn.on("frame", function(){
				if(sprtMOn.frame === sprtMOn.frames - 1)
				{
					sprtMOn.pause();
					$(teapotAnimated).fadeOut(0);
					$(teapotOff).fadeIn(0);
				}
			});
			sprtMOn.play();
		}
	});
	$(thermometer).css("background-size", (sprtTh.frames * 100) + "% 100%");

	$(teapotAnimated).css("background-size", "100% 100%");
	
	$(teapotOff).css("background-size", "100% 100%");
	$(teapotOn).css("background-size", "100% 100%");
	
	audio[0].addEventListener("ended", function(){
		sprtTh.on("frame", function(){
			if(sprtTh.frame === 1)
			{
				sprtTh.pause();
			}
		});
		sprtTh.play(true);
	});
	audio[1].addEventListener("ended", function(){
		theFrame.attr("data-done", "true"); 
		fadeNavsInAuto();
		sendCompletedStatement(14);
	});
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		audio[1].currentTime = 13;
		audio[1].play();
		timeout[0] = setTimeout(function(){
			audio[0].play();
			audio[0].volume = 0.5;
			$(teapotOff).fadeOut(0);
			$(teapotOn).fadeIn(0);
			sprtTh.play();
		}, 5000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(14);
	}, 2000);
}

launch["frame-115"] = function(){
	theFrame = $("#frame-115"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		balls = $(prefix + ".handle"),
		title = $(prefix + ".title");
	
	audio[0] = new Audio("audio/s13-1.mp3");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		basket.css("background-color", vegetable.css("background-color"));
		basket.css("color", vegetable.css("color"));
		
		vegetable.remove();
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css({
			"left": "",
			"top": ""
		});
	}
	
	var finishCondition = function()
	{
		return !$(".handle").length;
	}
	
	var finishFunction = function()
	{
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(15);
		}, 2000);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	balls.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	title.fadeOut(0);
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		audio[0].play();
		balls.fadeIn(1000);
		title.fadeIn(1000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(15);
	}, 2000);
}

launch["frame-116"] = function(){
	theFrame = $("#frame-116"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		balls = $(prefix + ".ball"),
		girl = $(prefix + ".girl");
	
	audioPieces[0] = new AudioPiece("s14-1", 0, 7);
	audioPieces[1] = new AudioPiece("s14-1", 10, 99);
	
	audioPieces[0].addEventListener("ended", function(){
		balls.fadeIn(1000);
	});
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		basket.css("border", "none");
		vegetable.remove();
	};
	
	var failFunction = function(vegetable, basket){
		vegetable.css({
			"left": "",
			"top": ""
		});
	};
	
	var finishCondition = function(){
		return !$(prefix + ".ball").length;
	};
	
	var finishFunction = function(){
		theFrame.attr("data-done", "true"); 
		audioPieces[1].play();
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true"); 
			fadeNavsInAuto();
			sendCompletedStatement(16);
		}, 14000);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	girl.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	balls.fadeOut(0);
	
	startButtonListener = function(){
		girl.fadeIn(1000);
		audioPieces[0].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(16);
	}, 2000);
}

launch["frame-201"] = function(){
	theFrame = $("#frame-201"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		oldMen = $(prefix + ".old-man"),
		oldManSad = $(prefix + ".old-man.sad"),
		oldManHappy = $(prefix + ".old-man.happy"),
		oldManNeutral = $(prefix + ".old-man.neutral"),
		tasks = $(prefix + ".task"),
		activeTask = 1,
		answers = $(prefix + ".answer"),
		correctNum = 0,
		balls = $(prefix + ".ball"),
		answerfields = $(prefix + ".answerfield"),
		checkButton = $(prefix + ".check-button"),
		result = $(prefix + ".task-15 .question"),
		timer = $(prefix + ".timer");
		
	oldMen.fadeOut(0);
	oldManNeutral.fadeIn(0);
	tasks.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	answerfields.val("");
	timer.html("");
		
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		vegetable.remove();
		basket.css("border", "5px solid green");
		oldManHappy.fadeIn(500);
		correctNum ++;
		result.html("Correct answers: " + correctNum + " ");
		
		timeout[0] = setTimeout(function(){
			oldManHappy.fadeOut(0);
			activeTask ++;
			tasks.fadeOut(0);
			$(".task-" + activeTask).fadeIn(0);
			blink2($(".ball"), 500, 2);
		}, 3000);
	};
	
	var failFunction = function(vegetable, basket)
	{
		if(basket.hasClass("basket"))
		{
			vegetable.remove();
			basket.css("border", "5px solid red");
			$(".task-" + activeTask + " .correct").css("border", "5px solid green");
			oldManSad.fadeIn(500);
			
			timeout[0] = setTimeout(function(){
				oldManSad.fadeOut(0);
				activeTask ++;
				tasks.fadeOut(0);
				$(".task-" + activeTask).fadeIn(0);
				blink2($(".ball"), 500, 2);
			}, 3000);
		}
	};
	
	var finishCondition = function()
	{
		
	};
	
	var finishFunction = function()
	{
		
	};
	
	var dragTask = DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	
	var checkButtonListener = function(){
		answerfield = $(".task-" + activeTask + " .answerfield");
		
		for(var i = 0; i < answerfield.length; i++)
		{
			currField = $(answerfield[i]);
			
			if(currField.val() === currField.attr("data-correct"))
			{
				currField.css("color", "green");
				correctNum ++;
				result.html("Correct answers: " + correctNum + ".");
				theFrame.attr("data-done", "true"); 
				fadeNavsIn();
			}
			else
			{
				currField.css("color", "red");
			}
		}
		
		timeout[2] = setTimeout(function(){
			for(var i = 0; i < answerfield.length; i++)
			{
				currField = $(answerfield[i]);
				
				currField.val(currField.attr("data-correct"));
				currField.css("color", "green");
			}
		}, 3000);
		
		timeout[3] = setTimeout(function(){
			activeTask ++;
			tasks.fadeOut(0);
			$(".task-" + activeTask).fadeIn(0);
			stopTimer();
		}, 6000);
	};
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	var answersListener = function(){
		currAnswer = $(this);
		if(currAnswer.hasClass("correct"))
		{
			oldManHappy.fadeIn(500);
			currAnswer.css("color", "green");
			correctNum ++;
			result.html("Correct answers: " + correctNum);
		}
		else
		{
			oldManSad.fadeIn(500);
			currAnswer.css("color", "red");
			$(".task-" + activeTask + " .correct").css("color", "green");
		}
		
		timeout[0] = setTimeout(function(){
			oldManSad.fadeOut(0);
			oldManHappy.fadeOut(0);
			activeTask ++;
			tasks.fadeOut(0);
			$(".task-" + activeTask).fadeIn(0);
			blink2($(".ball"), 500, 2);
		}, 2000);
	};
	answers.off("click", answersListener);
	answers.on("click", answersListener);
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true"); 
		$(".task-" + activeTask).fadeIn(0);
		startTimer(timer, 600, function(){
			activeTask = 15;
			tasks.fadeOut(0);
			$(".task-" + activeTask).fadeIn(0);
		})
	};
	
	startButtonListener();
};

launch["frame-301"] = function(){
	theFrame = $("#frame-301"),
	theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		text = $(prefix + ".text"),
		researchAnswer = $(prefix + ".research-answer");
	
	var textListener = function(){
		researchAnswer.fadeIn(500);
	};
	text.off("click", textListener);
	text.on("click", textListener);
	
	researchAnswer.fadeOut(0);
	
	startButtonListener = function(){		
	};
}

launch["frame-401"] = function(){
	theFrame = $("#frame-401"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		facts = $(prefix + ".fact");
		
	var factsListener = function(){
		var currFact = $(this);
		if(currFact.hasClass("center"))
		{
			currFact.css("color", "");
			
			timeout[0] = setTimeout(function(){
				currFact.removeClass("center");
			}, 1000);
		}
		else
		{
			facts.removeClass("center");
			facts.css("color", "");
			timeout[0] = setTimeout(function(){
				currFact.addClass("center");
			}, 500);
			timeout[1] = setTimeout(function(){
				$(".center").css("color", "black");
			}, 1000);
		}
	};
	facts.off("click", factsListener);
	facts.on("click", factsListener);
	
	facts.css("border", "2px solid green");
	timeout[0] = setTimeout(function(){
		facts.css("border", "");
	}, 4000);
	
	startButtonListener = function(){
		
	};
}

var hideEverythingBut = function(elem)
{
	if(theFrame)
		theFrame.html(theClone.html());
	
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	elemId = elem.attr("id");
	regBox = $(".reg-box");
	fadeTimerOut();
	
	if(!globalName)
		regBox.fadeOut(0);
	
	if(elemId === "frame-000")
	{
		fadeNavsOut();
		fadeTimerOut();
	}
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	
	
	for (var i = 0; i < audioPieces.length; i++)
		audioPieces[i].pause();	

	for (var i = 0; i < timeout.length; i++)
	{
		window.clearTimeout(timeout[i]);
	}
	
	for (var i = 0; i < sprite.length; i++)
		sprite[i].pause();
	
	for (var i = 0; i < typingText.length; i++)
		typingText[i].stopWriting();
	
	if(elem.attr("id") === "frame-000")
		regBox.fadeIn(0);
	
	if(elem.hasClass("fact"))
	{
		fadeNavsOut();
		fadeTimerOut();
	}
	
	launch[elemId]();
	initMenuButtons();
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link")), 
			elemId = elem.attr("id");
		
		launch[elemId]();
		
		hideEverythingBut(elem);
	});
};

var main = function()
{
	var video = $(".intro-video"),
		pic = $(".intro-pic");

	setLRSData();
	hideEverythingBut($("#frame-000"));
	setMenuStuff();
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));

	video[0].play();
	
	timeout[0] = setTimeout(function(){
		video.hide();
	}, 10000);
	timeout[1] = setTimeout(function(){
		pic.hide(); 
	}, 15000);


};

$(document).ready(main);